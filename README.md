# Easter Computus Resources
> Provides Easter dates for years 1951-2050 in structured yaml format
> and packaged as jar so it can be used as dependency.

See [easter-dates-1951-2050.yaml file](src/main/resources/computus/easter-dates-1951-2050.yaml)

## Installing / Getting started

To use most recent snapshot version of library in your project,
add this to your gradle file:

```gradle
dependencies {
    implementation 'com.gitlab.jaspery-public-libraries:easter-computus-resources:-SNAPSHOT'
}
```

There are no stable releases of this library yet.

## Features

For now it just contains
[easter-dates-1951-2050.yaml file](src/main/resources/computus/easter-dates-1951-2050.yaml)
which can be packaged into jar file as simply as running

```shell
> ./gradlew assemble
```

Here's how snippet of this file looks:

```yaml
Year,    Easter Date,     Julian Easter Date:
  - [1951, 1951-03-25,        1951-04-29]
  - [1952, 1951-04-13,        1951-04-20]
```

## Using

Simple [Kotlin](http://kotlinlang.org) code to parse Easter dates data from file
using [SnakeYAML](https://bitbucket.org/asomov/snakeyaml/) library capabilities:

```kotlin
val yamlParser = Yaml(Constructor(), Representer(), DumperOptions(), LoaderOptions(), object : Resolver() {
    override fun addImplicitResolvers() {
        // we want it to always resolve to string
        // super.addImplicitResolvers()
    }
})
val tree = yamlParser.load<Map<String, List<List<String>>>>(reader)
val easterDateFileLines: List<List<String>> = tree.values.singleOrNull() ?: error("Invalid format")
val r = easterDateFileLines.map { (year, date, dateJulian) -> "$year, $date, $dateJulian" }
```

## Deployment / Publishing

Code is hosted on [GitLab](https://gitlab.com/jaspery-public-libraries/easter-computus-resources)
and published via [JitPack](https://jitpack.io)

## Links

- Project homepage: https://gitlab.com/jaspery-public-libraries/easter-computus-resources
- Repository: https://gitlab.com/jaspery-public-libraries/easter-computus-resources
- Related projects:
  - TBD

## Licensing

Pre-release (snapshot) code in this project is licensed under GPL v3.0 license.
This may change in the future.

## About Author

Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
